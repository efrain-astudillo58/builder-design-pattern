using System.Runtime.InteropServices.JavaScript;

namespace BuilderDesignPattern.Composition;

public class Company
{
    public string? Id { get; }
    
    public string? Name { get; }
    
    public string? Address { get; }
    
    public string? Owner { get; }
    
    public string? Country { get; }
    
    public int? Employees { get; }
    
    public bool? IsCompetitor { get; }
}


public class Buyer {} 
public class Seller {}
public class CartItem {}

public class Order
{
    public int Id { get; set; }
    
    public DateTime CreatedAt { get; set; }
    
    public float Total { get; set; }

    public IList<Product> Products { get; set; }
    public Order(Buyer buyer, Seller seller, IList<CartItem> items, float discount) {}
}

public class Product
{
}
public interface IBuyerRepository
{
    Buyer GetById(string id);
}

public interface ISellerRepository
{
    Seller GetById(string id);
}

public interface ICartRepository
{
    IList<CartItem> GetById(string id);
}

public class OrderBuilder
{
    private readonly IBuyerRepository _buyerRepository;
    private readonly ISellerRepository _sellerRepository;
    private readonly ICartRepository _cartRepository;

    private Buyer _buyer;
    private Seller _seller;
    private IList<CartItem> _items = new List<CartItem>();
    private float _discount;
    
    public OrderBuilder(IBuyerRepository buyerRepository, ISellerRepository sellerRepository, ICartRepository cartRepository)
    {
        _buyerRepository = buyerRepository;
        _sellerRepository = sellerRepository;
        _cartRepository = cartRepository;
    }

    public OrderBuilder SetBuyer(string buyerId)
    {
        _buyer = _buyerRepository.GetById(buyerId);
        return this;
    }

    public OrderBuilder SetSeller(string sellerId)
    {
        _seller = _sellerRepository.GetById(sellerId);
        return this;
    }

    public OrderBuilder SetItems(string cartId)
    {
        _items = _cartRepository.GetById(cartId);

        return this;
    }

    public OrderBuilder WithDiscount(float discount)
    {
        _discount = discount;

        return this;
    }
    
    public Order Build()
    {
        return new Order(_buyer, _seller, _items, _discount);
    }
}
public class OrderService
{
    private readonly IBuyerRepository _buyerRepository;
    private readonly ISellerRepository _sellerRepository;
    private readonly ICartRepository _cartRepository;

    public OrderService(IBuyerRepository buyerRepository, ISellerRepository sellerRepository, ICartRepository cartRepository)
    {
        _buyerRepository = buyerRepository;
        _sellerRepository = sellerRepository;
        _cartRepository = cartRepository;
    }
    
    public Order CreateOrder(string buyerId, string sellerId, string cartId, float discount)
    {
        Buyer buyer = _buyerRepository.GetById(buyerId);
        Seller seller = _sellerRepository.GetById(sellerId);
        IList<CartItem> items = _cartRepository.GetById(cartId);

        // TODO: Add validations
        // ...
        
        return new Order(buyer, seller, items, discount);
    }

    public Order CreateOrderV2(string buyerId, string sellerId, string cartId, float discount)
    {
        var order = new OrderBuilder(this._buyerRepository, this._sellerRepository, this._cartRepository)
            .SetBuyer(buyerId)
            .SetSeller(sellerId)
            .SetItems(cartId)
            .Build();

        return order;
    }
}


