using BuilderDesignPattern.Utils;

namespace BuilderDesignPattern;

public class ConditionBuilder
{
    private IList<KeyValuePair<string, object?>> _equals = new List<KeyValuePair<string, object?>>();
    private IList<KeyValuePair<string, object?>> _in = new List<KeyValuePair<string, object?>>();

    public ConditionBuilder Eq(KeyValuePair<string, object?> value)
    {
        _equals.Add(value);
        
        return this;
    }

    public ConditionBuilder In(KeyValuePair<string, object?> value)
    {
        _in.Add(value);
        
        return this;
    }
    
    public string Build()
    {
        var conditions = new List<string>();
        
        if (!_equals.IsEmpty())
        {
            foreach (var (key, value) in _equals)
            {
                if (value is null)
                    continue;
                
                conditions.Add(this.ParseEqualCondition(key, (string)value));
            }
        }

        if (!_in.IsEmpty())
        {
            foreach (var (key, value) in _in)
            {
                if (value is null)
                    continue;

                conditions.Add(this.ParseInCondition(key, (IList<string>)value));
            }
        }
        
        return string.Join(" AND ",conditions);
    }
    
    private string ParseEqualCondition(string key, string value)
    {
        return  $"{key}='{value}'";
    }

    private string ParseInCondition(string key, IList<string> options)
    {
        return $"{key} IN ('{string.Join("','", options)}')";
    }

    
}