using BuilderDesignPattern.Utils;

namespace BuilderDesignPattern;

public class SqlQueryBuilder
{
    private string _query = "";
    private string _columns = "*";
    private string _table = "";
    private string _where = "";
    
    public SqlQueryBuilder Select(List<string> fields)
    {
        if (!fields.IsEmpty()) {
            _columns = string.Join(" ", fields);
        }

        return this;
    }

    public SqlQueryBuilder From(string table)
    {
        _table = table;

        return this;
    }

    public SqlQueryBuilder Where(string condition)
    {
        _where = condition;

        return this;
    }

    public string Build()
    {
        return $"SELECT {_columns} FROM {_table} WHERE {_where };";  
    }
}
