namespace BuilderDesignPattern;

// TODO: To demonstrate the problem of telescoping constructor.
public class VideoCard
{
    public int Memory { get; }

    public string Model { get; }
    
    public VideoCard()
    {
        Model = "NVidia";
        Memory = 2;
    }

    public VideoCard(string model)
    {
        Model = model;
        Memory = 2;
    }

    public VideoCard(int memory)
    {
        Memory = memory;
        Model = "NVidia";
    }

    public VideoCard(string model, int memory)
    {
        Memory = memory;
        Model = model;
    }
}
public class Computer
{
    private int _memory;
    private int _hardDrive;
    private string _processor;
    private VideoCard _videoCard;
    public Computer()
    {
        _memory = 16; // GB
        _hardDrive = 1000; // GB
        _processor = "i7";
        _videoCard = new VideoCard();
    }
    public Computer(string processor)
    {
        _processor = processor;
        _memory = 16; // GB
        _hardDrive = 1000; // GB
        _videoCard = new VideoCard();
    }
    public Computer(string processor, int memory)
    {
        _memory = memory; // GB
        _processor = processor;
        _hardDrive = 1000; // GB
        _videoCard = new VideoCard();
    }
    public Computer(string processor, int memory, int hardDrive)
    {
        _memory = memory; // GB
        _processor = processor;
        _hardDrive = hardDrive; // GB
        _videoCard = new VideoCard();
    }
    public Computer(string processor, int memory, int hardDrive, string graphicsCardModel)
    {
        _memory = memory; // GB
        _processor = processor;
        _hardDrive = hardDrive; // GB
        _videoCard = new VideoCard(graphicsCardModel);
    }
    public Computer(string processor, int memory, int hardDrive, string graphicsCardModel, int graphicsCardSize)
    {
        _memory = memory; // GB
        _processor = processor;
        _hardDrive = hardDrive; // GB
        _videoCard = new VideoCard(graphicsCardModel, graphicsCardSize);
    }
}
