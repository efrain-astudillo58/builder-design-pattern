using System.Linq.Expressions;
using System.Text;
using BuilderDesignPattern.Utils;

namespace BuilderDesignPattern.Components;

public class SqlServerQueryQueryBuilder<TSource>: IQueryBuilder<TSource>
{
    private IList<string> _columns = new List<string>();
    private string _orderBy = "";

    public IQueryBuilder<TSource> Select(params Expression<Func<TSource, object?>>[]? args)
    {
        if (args is null || args.IsEmpty())
        {
            return this;
        }

        foreach (var func in args)
        {
            var columnName = GetPropertyName(func);

            _columns.Add(columnName);
        }

        return this;
    }
    public IQueryBuilder<TSource> Where(Expression<Func<TSource, bool>> predicate)
    {
        if (predicate.Body is not BinaryExpression parameter)
            throw new ApplicationException("Parameter is null");

        var left = ((MemberExpression)parameter.Left).Member.Name;
        // var rigth = ((UnaryExpression)parameter.Right);

        // TODO: read the expression to convert to a boolean counterpart in SQL
        return this;
    }
    public IQueryBuilder<TSource> OrderBy(Expression<Func<TSource, object?>> orderBy, string direction)
    {
        var orderColumn = GetPropertyName(orderBy);

        _orderBy += "ORDER BY " + orderColumn + " " + direction;

        return this;
    }
    public string Build()
    {
        var stringBuilder = new StringBuilder("SELECT ");

        stringBuilder.Append(!_columns.IsEmpty() ? string.Join(", ", _columns) : "*");

        stringBuilder.Append(" FROM Companies ");

        if (!string.IsNullOrEmpty(_orderBy))
        {
            stringBuilder.Append(_orderBy);
        }

        return stringBuilder.ToString();
    }
    private static string GetPropertyName<TReturn>(Expression<Func<TSource, TReturn>>? expression)
    {
        if (expression is null)
            throw new ApplicationException("Expression is null");

        if (expression.Body is MemberExpression parameter)
        {
            return parameter.Member.Name;
        }

        if (expression.Body is UnaryExpression unary)
        {
            return ((MemberExpression)unary.Operand).Member.Name;
        }

        return "";
    }
}