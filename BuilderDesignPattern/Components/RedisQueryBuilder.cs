using System.Linq.Expressions;
using System.Text;

namespace BuilderDesignPattern.Components;

public class RedisQueryBuilder<TSource>: IQueryBuilder<TSource>
{
    private string _columns = "";
    private string _where = "";
    
    public IQueryBuilder<TSource> Select(params Expression<Func<TSource, object?>>[]? args)
    {
        _columns = "FT.SEARCH orders ";
        return this;
    }

    public IQueryBuilder<TSource> Where(Expression<Func<TSource, bool>> predicate)
    {
        _where = "@num:[-inf (10]";

        return this;
    }

    public IQueryBuilder<TSource> OrderBy(Expression<Func<TSource, object?>> orderBy, string direction)
    {
        return this;
    }

    public string Build()
    {
        var builder = new StringBuilder();

        builder.Append(_columns);
        builder.Append(_where);

        return builder.ToString();
    }
}