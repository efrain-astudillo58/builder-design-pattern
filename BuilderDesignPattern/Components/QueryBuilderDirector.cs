using BuilderDesignPattern.Composition;

namespace BuilderDesignPattern.Components;

public static class QueryBuilderDirector
{
    public static IQueryBuilder<Company> GetCompaniesInUsa()
    {
        return new SqlServerQueryQueryBuilder<Company>()
            .Select(t => t.Id, t => t.Name, t => t.Country)
            .Where(t => t.Name == "USA")
            .OrderBy(t => t.Name, "ASC");
    }

    public static IQueryBuilder<Company> GetCompaniesWithEmployeesMoreThan(int count)
    {
        return new SqlServerQueryQueryBuilder<Company>()
            .Select(t => t.Id, t => t.Name, t => t.Country, t => t.IsCompetitor, t => t.Address)
            .Where(t => t.Employees > count)
            .OrderBy(t => t.Name, "ASC");
    }

    public static IQueryBuilder<Order> GetOrdersGreaterThan(float total)
    {
        return new RedisQueryBuilder<Order>()
            .Select(t => t.Id,t=> t.Products, t => t.CreatedAt, t => t.Total)
            .Where(t => t.Total > total)
            .OrderBy(t => t.CreatedAt, "ASC");
    }
}