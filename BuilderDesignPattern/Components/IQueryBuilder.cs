using System.Linq.Expressions;

namespace BuilderDesignPattern.Components;

public interface IQueryBuilder<TSource>
{
    IQueryBuilder<TSource> Select(params Expression<Func<TSource, object?>>[]? args);
    IQueryBuilder<TSource> Where(Expression<Func<TSource, bool>> predicate);
    IQueryBuilder<TSource> OrderBy(Expression<Func<TSource, object?>> orderBy, string direction);
    string Build();
}