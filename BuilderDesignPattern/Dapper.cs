using System.Collections.ObjectModel;
using System.Data;

namespace BuilderDesignPattern;

public interface IConnection: IDisposable
{
    Task<IList<TSource>> QueryAsync<TSource>(string query);
}

public interface IDatabase
{
    void Connect(string connectionString);
    void Close();
}
public class Database: IDatabase
{
    public void Connect(string connectionString)
    {
        Console.WriteLine("Opening database connection...");
    }

    public void Close()
    {
        Console.WriteLine("Closing database connection...");
    }
}

public interface IDbContext
{
    IConnection CreateConnection();
    void Shutdown();
}

public class DbContext: IDbContext
{
    private readonly IDatabase _database;
    private readonly string _url;

    public DbContext(string connectionString)
    {
        _url = connectionString;
        _database = new Database();
    }
    
    public IConnection CreateConnection()
    {
        _database.Connect(_url);
        
        return new DapperConnection(this);
    }

    public void Shutdown()
    {
        _database.Close();
    }
}

public class DapperConnection: IConnection
{
    private DbContext _context;

    public DapperConnection(DbContext context)
    {
        _context = context;
    }
    
    public Task<IList<TSource>> QueryAsync<TSource>(string query)
    {
        Console.WriteLine($"QUERY: {query}");

        return Task.FromResult<IList<TSource>>(new List<TSource>());
    }

    public void Dispose()
    {
        _context.Shutdown();
    }   
}