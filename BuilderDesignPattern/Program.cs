﻿// See https://aka.ms/new-console-template for more information

using BuilderDesignPattern;
using BuilderDesignPattern.Components;
using BuilderDesignPattern.Composition;

IDbContext context = new DbContext("mongo://link.to.a.datasource:999");


Console.WriteLine(" =====> Get Companies SQL Query.");
var repository = new CompanyRepository(context);
var companies = await repository.GetCompanies();

#region Using Director.

IQueryBuilder<Company> sqlQueryBuilder = QueryBuilderDirector.GetCompaniesWithEmployeesMoreThan(4);
IQueryBuilder<Order> redisQueryQueryBuilder = QueryBuilderDirector.GetOrdersGreaterThan(200.3f);

Console.WriteLine(" =====> Get Companies With Employees More than 4 SQL Query.");
var companiesList = await repository.GetCompanies2(sqlQueryBuilder);

Console.WriteLine(" =====> Get Orders REDIS Query.");
var orders = await repository.GetOrders(redisQueryQueryBuilder);

#endregion endregion