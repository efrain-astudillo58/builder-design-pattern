using BuilderDesignPattern.Components;
using BuilderDesignPattern.Composition;

namespace BuilderDesignPattern;

public class CompanyRepository
{
    private readonly IDbContext _context;

    public CompanyRepository(IDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Company>> GetCompanies2(IQueryBuilder<Company> queryBuilder)
    {
        var query = queryBuilder.Build();
        
        using var connection = _context.CreateConnection();
        
        return await connection.QueryAsync<Company>(query);
    }

    public async Task<IEnumerable<Order>> GetOrders(IQueryBuilder<Order> queryBuilder)
    {
        var query = queryBuilder.Build();
        
        using var connection = _context.CreateConnection();
        
        return await connection.QueryAsync<Order>(query);
    }
    
    public async Task<IEnumerable<Company>> GetCompanies()
    {
        var query = new SqlServerQueryQueryBuilder<Company>()
            .Select(t => t.Id, t => t.Name, t => t.Country)
            .Where(t => t.Name == "USA")
            .OrderBy(t => t.Name, "ASC")
            .Build();
        
        using var connection = _context.CreateConnection();
        
        return await connection.QueryAsync<Company>(query);
    }
}














//  
// public async Task<IEnumerable<Company>> GetCompaniesRawString(string country)
// {
//     var query = 
//         $"""
//          SELECT
//              Id,
//              Name AS CompanyName,
//              Address,
//              Country,
//              State
//          FROM Companies
//          WHERE Country='{country}'
//          """;
//          
//     using (var connection = _context.CreateConnection())
//     {
//         var companies = await connection.QueryAsync<Company>(query);
//              
//         return companies;
//     }
// }






// public async Task<IEnumerable<Company>> GetCompaniesSimple()
// {
//     var countryIsUsa = new ConditionBuilder()
//         .Eq(KeyValuePair.Create<string, object?>("Country", "USA"))
//         .Eq(KeyValuePair.Create<string, object?>("Name", "Valerio"))
//         .In(KeyValuePair.Create<string, object?>("Id", new List<string> { "1", "2", "3" }))
//         .Build();
//
//     var query = new SqlQueryBuilder()
//         .Select(new List<string> {"Id", "Name AS CompanyName", "Address", "Country" })
//         .From("Companies")
//         .Where(countryIsUsa)
//         .Build();
//
//     using var connection = _context.CreateConnection();
//         
//     return await connection.QueryAsync<Company>(query);
// }
